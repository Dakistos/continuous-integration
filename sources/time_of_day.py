from datetime import datetime


def get_time_of_day():
    now = datetime.now()
    time = int(now.strftime("%H"))
    # time = 19

    if 0 <= time < 6:
        return "Bonne nuit"
    if 6 <= time < 12:
        return "Café du matin"
    if 12 <= time < 18:
        return "Bon après midi"

    return "Bonne soirée"


print(get_time_of_day())
